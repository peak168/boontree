import React, { useEffect, useRef, useState } from "react";
import {
  OTPInputContainer,
  SplitBoxes,
  SplitBoxesFocused,
  SplitBoxText,
  SplitOTPBoxesContainer,
  TextInputHidden,
} from "./Styles";

const OTPInput = ({
  code,
  setCode,
  maximumLength,
  setIsPinReady,
}: {
  code: string;
  setCode: (code: string) => void;
  maximumLength: number;
  setIsPinReady: (isPinReady: boolean) => void;
}) => {
  const boxArray = new Array(maximumLength).fill(0);
  const inputRef = useRef();
  const [isInputBoxFocused, setIsInputBoxFocused] = useState(false);

  const boxDigit = (_: any, index: number) => {
    const emptyInput = "";
    const digit = code[index] || emptyInput;

    const isCurrentValue = index === code.length;
    const isLastValue = index === maximumLength - 1;
    const isCodeComplete = code.length === maximumLength;

    const isValueFocused = isCurrentValue || (isLastValue && isCodeComplete);

    const StyledSplitBoxes =
      isInputBoxFocused && isValueFocused ? SplitBoxesFocused : SplitBoxes;
    return (
      <StyledSplitBoxes key={index}>
        <SplitBoxText isActive={isInputBoxFocused && isValueFocused}>
          {digit}
        </SplitBoxText>
      </StyledSplitBoxes>
    );
  };

  const handleOnPress = () => {
    setIsInputBoxFocused(true);
    inputRef?.current?.focus();
  };

  const handleOnBlur = () => {
    setIsInputBoxFocused(false);
  };

  useEffect(() => {
    // update pin ready status
    setIsPinReady(code.length === maximumLength);
    // clean up function
    return () => {
      setIsPinReady(false);
    };
  }, [code]);

  return (
    <OTPInputContainer>
      <SplitOTPBoxesContainer onPress={handleOnPress}>
        {boxArray.map(boxDigit)}
      </SplitOTPBoxesContainer>
      <TextInputHidden
        value={code}
        onChangeText={setCode}
        maxLength={maximumLength}
        ref={inputRef}
        onBlur={handleOnBlur}
        keyboardType="number-pad"
      />
    </OTPInputContainer>
  );
};

export default OTPInput;
