import styled from "styled-components/native";

export const OTPInputContainer = styled.View`
  justify-content: center;
  align-items: center;
`;

export const TextInputHidden = styled.TextInput`
  position: absolute;
  opacity: 0;
`;

export const SplitOTPBoxesContainer = styled.Pressable`
  width: 80%;
  flex-direction: row;
  justify-content: space-evenly;
`;

export const SplitBoxes = styled.View`
  border-color: #e5e5e5;
  border-width: 2px;
  border-radius: 5px;
  min-width: 40px;
  min-height: 40px;
`;

export const SplitBoxText = styled.Text<{ isActive: boolean }>`
  font-size: 20px;
  text-align: center;
  color: ${({ isActive }) => (isActive ? "#fff" : "#000")};
`;

export const SplitBoxesFocused = styled(SplitBoxes)`
  border-color: #ffea00;
  background-color: #423efd;
`;

export const ButtonContainer = styled.TouchableOpacity`
  padding: 12px;
  justify-content: center;
  align-items: center;
  width: 150px;
  margin-top: 30px;
  border-radius: 4px;
`;

export const ButtonText = styled.Text`
  color: #423efd;
  font-size: 20px;
`;
