// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import "firebase/auth";
import Constants from "expo-constants";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAZKmGtvoCDfr-hSBHYRlFcB5ruK7FdQGo",
  authDomain: "boontree-f2c35.firebaseapp.com",
  projectId: "boontree-f2c35",
  storageBucket: "boontree-f2c35.appspot.com",
  messagingSenderId: "642456849034",
  appId: "1:642456849034:web:37899b15384eb18ebcfb73",
  measurementId: "G-04FRJJ81C4",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;
