import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import WelcomeScreen from "../screens/Welcome";
import SignInScreen from "../screens/SignInScreen";
import SignUpScreen from "../screens/SignUpScreen";
import OtpScreen from "../screens/OtpScreen";
import PinScreen from "../screens/PinScreen";

const Stack = createStackNavigator();

export default function AuthStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Welcome"
          options={{ title: "พระต้นธาตุต้นธรรม" }}
          component={WelcomeScreen}
        />
        <Stack.Screen
          name="Pin"
          options={{ title: "เข้าร่วมกลุ่ม" }}
          component={PinScreen}
        />
        <Stack.Screen
          name="Sign In"
          options={{
            title: "เข้าสู่ระบบ",
          }}
          component={OtpScreen}
        />
        <Stack.Screen
          name="Sign Up"
          options={{
            title: "สมัครสมาชิก",
          }}
          component={SignUpScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
