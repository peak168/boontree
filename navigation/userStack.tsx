import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import HomeScreen from "../screens/Home";
import { Icon } from "@ui-kitten/components";

const Stack = createStackNavigator();

const MenuIcon = (props: any) => <Icon {...props} name="more-vertical" />;

export default function UserStack() {
  const [showRightMenu, setShowRightMenu] = React.useState(false);
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          options={{
            title: "พระต้นธาตุต้นธรรม",
            headerRight: () => {
              return <MenuIcon />;
            },
          }}
          component={HomeScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
