import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { useAuthentication } from "../utils/hooks/useAuthentication";
// import { Button } from "react-native-elements";
import {
  Button,
  Icon,
  Layout,
  MenuItem,
  OverflowMenu,
  TopNavigation,
  TopNavigationAction,
} from "@ui-kitten/components";
import { getAuth, signOut } from "firebase/auth";
import { formatPrefixGlobalToThaiFormat } from "../utils/hooks/converter";
import { useNavigation } from "@react-navigation/native";

const MenuIcon = (props: any) => <Icon {...props} name="more-vertical" />;

const InfoIcon = (props: any) => <Icon {...props} name="info" />;

const LogoutIcon = (props: any) => <Icon {...props} name="log-out" />;

export default function HomeScreen() {
  const { user } = useAuthentication();

  const auth = getAuth();

  const nav = useNavigation();

  useEffect(() => {
    // nav.setOptions({
    //   headerRight: () => {
    //     return (
    //       <OverflowMenu
    //         anchor={renderMenuAction}
    //         visible={menuVisible}
    //         onSelect={() => {}}
    //         placement="bottom end"
    //       >
    //         <MenuItem accessoryLeft={InfoIcon} title="About" />
    //         <MenuItem accessoryLeft={LogoutIcon} title="Logout" />
    //       </OverflowMenu>
    //     );
    //   },
    // });
    return () => {};
  }, []);

  const [menuVisible, setMenuVisible] = React.useState(false);
  const toggleMenu = () => {
    setMenuVisible(!menuVisible);
  };

  const renderMenuAction = () => (
    <TopNavigationAction icon={MenuIcon} onPress={toggleMenu} />
  );

  const renderRightActions = () => (
    <React.Fragment>
      <OverflowMenu
        anchor={renderMenuAction}
        visible={menuVisible}
        onBackdropPress={toggleMenu}
      >
        <MenuItem accessoryLeft={InfoIcon} title="About" />
        <MenuItem accessoryLeft={LogoutIcon} title="Logout" />
      </OverflowMenu>
    </React.Fragment>
  );

  return (
    <View style={styles.container}>
      {/* <Layout style={styles.layoutContainer} level="1"> */}
      {/* <TopNavigation
        alignment="center"
        title="Eva Application"
        subtitle="Subtitle"
        accessoryRight={renderRightActions}
      /> */}
      {/* </Layout> */}

      <Text>
        สวัสดี{" "}
        {user?.email || formatPrefixGlobalToThaiFormat(user?.phoneNumber || "")}{" "}
        !
      </Text>

      <Button style={styles.button} onPress={() => signOut(auth)}>
        ออกจากระบบ
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  layoutContainer: {
    minHeight: 128,
  },

  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    paddingTop: "60%",
    // justifyContent: "center",
  },
  button: {
    marginTop: 20,
  },
});
