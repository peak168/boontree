import { StackScreenProps } from "@react-navigation/stack";
import { StatusBar } from "expo-status-bar";
import { getAuth, signInAnonymously } from "firebase/auth";
import React, { useState } from "react";
import { Keyboard, Pressable, StyleSheet, Text } from "react-native";
import OTPInput from "../components/OTPInput";
import { ButtonContainer, ButtonText } from "../components/Styles";

const PinScreen: React.FC<StackScreenProps<any>> = ({ navigation }) => {
  const [otpCode, setOTPCode] = useState("");
  const [isPinReady, setIsPinReady] = useState(false);
  const maximumCodeLength = 6;
  const auth = getAuth();

  const handleOnPress = () => {
    signInAnonymously(auth)
      .then(() => {
        // Signed in..
      })
      .catch((error: any) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        // ...
      });
  };

  return (
    <Pressable style={styles.container} onPress={Keyboard.dismiss}>
      <Text style={{ width: "70%", marginBottom: 20, fontSize: 16 }}>
        กรุณากรอกรหัส 6 หลักที่ได้จากพี่เลี้ยง
      </Text>
      <OTPInput
        code={otpCode}
        setCode={setOTPCode}
        maximumLength={maximumCodeLength}
        setIsPinReady={setIsPinReady}
      />
      <ButtonContainer
        disabled={!isPinReady}
        style={{
          backgroundColor: !isPinReady ? "grey" : "#423efd",
        }}
        onPress={handleOnPress}
      >
        <ButtonText
          style={{
            color: !isPinReady ? "#cccccc" : "#EEEEEE",
          }}
        >
          เข้าร่วมกลุ่ม
        </ButtonText>
      </ButtonContainer>
      <StatusBar style="auto" />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    alignItems: "center",
    paddingTop: "10%",
  },
});

export default PinScreen;
