import React from "react";
import { StyleSheet, Text, TouchableWithoutFeedback, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
// import { Input } from "react-native-elements";
import { StackScreenProps } from "@react-navigation/stack";
import { createUserWithEmailAndPassword, getAuth } from "firebase/auth";
import { Button, Input } from "@ui-kitten/components";

const SignUpScreen: React.FC<StackScreenProps<any>> = ({ navigation }) => {
  const [value, setValue] = React.useState({
    email: "",
    password: "",
    error: "",
  });
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);

  const auth = getAuth();

  async function signUp() {
    if (value.email === "" || value.password === "") {
      setValue({
        ...value,
        error: "Email and password are mandatory.",
      });
      return;
    }

    try {
      await createUserWithEmailAndPassword(
        auth,
        value.email.toLowerCase(),
        value.password
      );
      navigation.navigate("Sign In");
    } catch (error: any) {
      setValue({
        ...value,
        error: error.message,
      });
    }
  }

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = (props: any) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon
        {...props}
        style={styles.controlIcon}
        name={secureTextEntry ? "eye-slash" : "eye"}
      />
    </TouchableWithoutFeedback>
  );

  return (
    <View style={styles.container}>
      {!!value.error && (
        <View style={styles.error}>
          <Text>{value.error}</Text>
        </View>
      )}

      <View style={styles.controls}>
        <Input
          // autoCompleteType={"email"}
          placeholder="Email"
          // containerStyle={styles.control}
          style={styles.control}
          value={value.email}
          onChangeText={(text) => setValue({ ...value, email: text })}
          // leftIcon={<Icon name="envelope" size={16} />}
        />

        <Input
          // autoCompleteType={"password"}
          placeholder="Password"
          // containerStyle={styles.control}
          style={styles.control}
          value={value.password}
          onChangeText={(text) => setValue({ ...value, password: text })}
          accessoryRight={renderIcon}
          secureTextEntry={secureTextEntry}
          // leftIcon={<Icon name="key" size={16} />}
        />

        <Button style={styles.control} onPress={signUp}>
          Sign up
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  controls: {
    flex: 1,
    width: "80%",
  },

  control: {
    marginTop: 10,
  },

  controlIcon: {
    fontSize: 16,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },

  error: {
    marginTop: 10,
    padding: 10,
    color: "#fff",
    backgroundColor: "#D54826FF",
  },
});

export default SignUpScreen;
