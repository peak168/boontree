import React, { useCallback } from "react";
import { Alert, Image, Linking, StyleSheet, Text, View } from "react-native";
import { StackScreenProps } from "@react-navigation/stack";
import { Button, Input } from "@ui-kitten/components";
// import { Button } from "react-native-elements";

const LOGO_URI =
  "https://firebasestorage.googleapis.com/v0/b/boontree-f2c35.appspot.com/o/logo.png?alt=media&token=5faffa5e-bcba-4c79-9efb-873ecc605c05";

const DECORATE_URI =
  "https://firebasestorage.googleapis.com/v0/b/boontree-f2c35.appspot.com/o/asset-2.png?alt=media&token=3c140ccf-2f63-47a5-b4a0-bdb266a73869";

const supportedURL = "https://google.com";

const WelcomeScreen: React.FC<StackScreenProps<any>> = ({ navigation }) => {
  const [value, setValue] = React.useState("");
  const handlePress = useCallback(async () => {
    // Checking if the link is supported for links with custom URL scheme.
    const supported = await Linking.canOpenURL(supportedURL);

    if (supported) {
      // Opening the link with some app, if the URL scheme is "http" the web link should be opened
      // by some browser in the mobile
      await Linking.openURL(supportedURL);
    } else {
      Alert.alert(`Don't know how to open this URL: ${supportedURL}`);
    }
  }, []);
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={{
          uri: LOGO_URI,
        }}
      />
      <Text style={styles.titleText}>โครงงานพระต้นธาตุต้นธรรม</Text>
      <Input
        style={styles.input}
        placeholder="รหัสนักเรียนธรรม"
        value={value}
        onChangeText={(nextValue) => setValue(nextValue)}
      />
      <Button
        style={styles.button}
        onPress={() => navigation.navigate("Pin")}
        size="large"
      >
        เข้าร่วมกลุ่ม
      </Button>

      <View
        style={{
          marginTop: 20,
          width: "80%",
          flexDirection: "row",
          flexWrap: "wrap",
          justifyContent: "space-between",
        }}
      >
        <Text style={{ fontSize: 18 }}>ไม่สามารถเข้าร่วมกลุ่มได้?</Text>
        <Text style={{ fontSize: 18, color: "#423EFD" }} onPress={handlePress}>
          ติดต่อพี่เลี้ยง?
        </Text>
      </View>

      <View style={styles.decorateImageView}>
        <Image style={styles.decorateImage} source={{ uri: DECORATE_URI }} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingTop: "10%",
    backgroundColor: "#fff",
  },

  titleText: {
    fontSize: 26,
    fontWeight: "bold",
  },

  input: {
    width: "80%",
    marginTop: 20,
  },

  text: {
    fontSize: 30,
    fontWeight: "bold",
  },

  button: {
    width: "80%",
    marginTop: 20,
  },

  image: {
    width: 68 * 2,
    height: 56 * 2,
  },

  decorateImageView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  decorateImage: {
    width: 300,
    height: 230,
    resizeMode: "contain",
  },
});

export default WelcomeScreen;
