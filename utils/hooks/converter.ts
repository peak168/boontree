export const formatPrefixGlobalToThaiFormat = (value: string) => {
  return value.replace("+66", "0");
};

export const formatPrefixThaiToGlobalFormat = (value: string) => {
  return value.replace("0", "+66");
};
